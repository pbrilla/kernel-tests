# storage/iscsi/params suite
Testing various functions of targetcli including use of supported backstore types, attributes or CHAP authentication   
The source code can be found at [python-stqe](https://gitlab.com/rh-kernel-stqe/python-stqe).  

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
